#!/bin/bash

#   any marker with missing rate lower than 95% is probably just bad overall and
#   should not be counted towards individual missing rate
#source config.sh

####################
# Preprocess       #
####################
#Grab the filename of this bash script
me="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"

Help()
{
    # Display Help
    echo
    echo "Usage: sh $me -i <file> -r <file> -f <file> [-p <PLINK_CMD>|-l <PERL_CMD>|-b <BCFTOOLS_CMD>|-y <PYTHON_CMD>|-d <dir>|-h]"
    echo
    echo "options:"
    echo "-i     Genotype files (<file>.ped + <file>.map)."
    echo "-r     HRC refernce file (HRC.r1-1.GRCh37.wgs.mac5.sites.tab)."
    echo "-f     Reference genome FASTA."
    echo "-p     PLINK executable file."
    echo "-l     Perl executable file."
    echo "-b     bcftools executable file."
    echo "-y     python executable file."
    echo "-d     Output directory."
    echo "-h     Print this Help."
    echo
}

#Arguments
while getopts i:r:f:p:l:b:y:d:h option
do
    case "${option}" in
        i) ifile=${OPTARG};;
	r) hrcref=${OPTARG};;
	f) ref=${OPTARG};;
	p) PLINKCMD=${OPTARG};;
	l) PERLCMD=${OPTARG};;
	b) BCFTOOLCMD=${OPTARG};;
	y) PYTHONCMD=${OPTARG};;
        d) dir=${OPTARG};;
        h) # display Help
            Help
            exit;;
    esac
done

#Assign default parameters
if [ -z $PLINKCMD ]; then
    PLINKCMD=$(command -v plink)
fi

if [ -z $PERLCMD ]; then
    PERLCMD=$(command -v perl)
fi

if [ -z $BCFTOOLCMD ]; then
    BCFTOOLCMD=$(command -v bcftools)
fi

if [ -z $PYTHONCMD ]; then
    PYTHONCMD=$(command -v python)
fi

if [ -z $dir ]; then
    dir="$(pwd)"
fi

#Check parameters
help=0
if [ -z $ifile ]; then
    echo "ERROR: Please enter -i <file>."
    help=1
fi

if [ -z $hrcref ]; then
    echo "ERROR: Please enter -r <file>."
    help=1
fi

if [ -z $ref ]; then
    echo "ERROR: Please enter -f <file>."
    help=1
fi

if ! [ -x "$PLINKCMD" ]; then
  echo "ERROR: plink is not installed."
  echo "       Or, Please specify -p <PLINK_CMD>."
  help=1
fi

if ! [ -x "$PERLCMD" ]; then
  echo "ERROR: perl is not installed."
  echo "       Or, Please specify -l <PERL_CMD>."
  help=1
fi

if ! [ -x "$BCFTOOLCMD" ]; then
  echo "ERROR: bcftools is not installed."
  echo "       Or, Please specify -b <BCFTOOLS_CMD>."
  help=1
fi

if ! [ -x "$PYTHONCMD" ]; then
  echo "ERROR: python is not installed."
  echo "       Or, Please specify -y <PYTHON_CMD>."
  help=1
fi

if ! [ -w "$dir" ]; then
  echo "ERROR: $dir is not a writable directory."
  echo "       Please specify -d <dir>."
  help=1
fi

if [ $help -eq 1 ]; then
  Help
  exit 1
fi

####################
# Main: 5 steps    #
####################

CHECK_BIM=$(dirname "$(realpath $0)")/../HRC_1000G_check_bim_v4.3.0/HRC_1000G_check_bim.pl
CHECK_VCF=$(dirname "$(realpath $0)")/../checkVCF/checkVCF.py

####################
echo 'Step 1: Convert genotype files (ped and map) into PLINK binary fileset'

#--bfile flag causes the binary fileset <PREFIX>.bed + <PREFIX>.bim + <PREFIX>.fam to be referenced.
#--chr0-13. Chr0 is unknown while Chr1-22 are autosomes.
#--output-chr lets you specify a different coding scheme by providing the desired human mitochondrial code; 
#  supported options are '26' (default), 'M', 'MT', '0M', 'chr26', 'chrM', and 'chrMT'.
#  (PLINK 1.9 correctly interprets all of these encodings in input files.)

$PLINKCMD --file ${ifile} --chr 1-23 --output-chr 26 --make-bed --out ${dir}/$(basename $ifile)_auto_sex

####################
echo 'Step 2: Create a frequency file'

mkdir ${dir}/freq

$PLINKCMD --bfile ${dir}/$(basename $ifile)_auto_sex --freq --out ${dir}/freq/$(basename $ifile)_auto_sex

####################
echo 'Step 3: HRC check' 

$PERLCMD $CHECK_BIM -b ${dir}/$(basename $ifile)_auto_sex.bim -f ${dir}/freq/$(basename $ifile)_auto_sex.frq -r $hrcref -h
sed -i "s+plink+$PLINKCMD+g" ${dir}/Run-plink.sh
pushd ${dir}
sh ${dir}/Run-plink.sh
popd
cat ${dir}/$(basename $ifile)_auto_sex.hh | awk -F'\t' '{print $3}' > ${dir}/exclude_hh.txt

####################
echo 'Step 4: Convert to VCF and sort'

mkdir ${dir}/vcf

for chr in $(seq 1 23);
do
  $PLINKCMD --bfile ${dir}/$(basename $ifile)_auto_sex-updated --real-ref-alleles --recode vcf --chr $chr --out ${dir}/$(basename $ifile)_auto_sex-updated-chr$chr
  $BCFTOOLCMD sort ${dir}/$(basename $ifile)_auto_sex-updated-chr$chr.vcf -Oz -o ${dir}/vcf/$(basename $ifile)_auto_sex-chr$chr.vcf.gz
done

####################
echo 'Step 5: Check VCF'

mkdir ${dir}/checkvcf
for chr in $(seq 1 23);
do
  $PYTHONCMD $CHECK_VCF -r $ref -o ${dir}/checkvcf/$(basename $ifile)_auto_sex-chr$chr\_out ${dir}/vcf/$(basename $ifile)_auto_sex-chr$chr.vcf.gz
done
